module.exports = {
  i18n: {
    locales: ["en"],
    defaultLocale: "en",
  },
  output: "standalone",
  images: {
    domains: ["cdn.idntimes.com", "cdn.dummyjson.com"],
  },
};
