### 1\. Install dependencies

```
yarn install
# or
npm install
```

### 2\. Run Dev Server

```
yarn dev
# or
npm run dev
```
