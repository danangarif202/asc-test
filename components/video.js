import { useState } from "react";
import Container from "./container";

const Video = () => {
  const [playVideo, setPlayVideo] = useState(false);
  return (
    <div className="relative bg-[#0d0c1e]">
      <Container className="max-w-6xl">
        <div className="flex w-full mt-4">
          <div className="flex w-1/3 mt-3 leading-snug tracking-tight text-gray-500 lg:leading-tight pr-10">
            <p className="w-full font-bold text-xl lg:text-6xl pr-5 text-white">
              11k+
            </p>
            <p className="text-xl hidden lg:block">
              Happy Customers with our services
            </p>
          </div>
          <div className="flex w-1/3 mt-3 leading-snug tracking-tight text-gray-500 lg:leading-tight pr-10">
            <p className="font-bold text-xl lg:text-6xl pr-5 text-white">
              22k+
            </p>
            <p className="text-xl hidden lg:block">
              Happy Customers with our services
            </p>
          </div>
          <div className="flex w-1/3 mt-3 leading-snug tracking-tight text-gray-500 lg:leading-tight pr-10">
            <p className="font-bold text-xl lg:text-6xl pr-5 text-white">
              520+
            </p>
            <p className="text-xl hidden lg:block">
              Happy Customers with our services
            </p>
          </div>
        </div>
        <div className="flex w-full mt-16 mb-10 border-t pt-10">
          <h2 className="w-1/2 mt-3 text-3xl font-bold leading-snug tracking-tight text-white lg:leading-tight lg:text-4xl pr-10">
            Where Comfort Meets Convenience
          </h2>
          <p className="w-1/2 py-4 text-lg leading-normal text-gray-500 lg:text-xl xl:text-xl">
            Look for an agency with a proven track record of success in buying,
            selling, or renting properties. you want an agency that has been in
            the industry for a while and has experience.
          </p>
        </div>
        <div className="w-full max-h-full mx-auto overflow-hidden lg:mb-20 rounded-2xl ">
          <div
            onClick={() => setPlayVideo(!playVideo)}
            className="relative bg-indigo-300 cursor-pointer aspect-w-16 aspect-h-9 bg-gradient-to-tr from-purple-400 to-indigo-700"
          >
            {!playVideo && (
              <button className="absolute inset-auto w-16 h-16 text-white transform -translate-x-1/2 -translate-y-1/2 lg:w-28 lg:h-28 top-1/2 left-1/2">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="w-16 h-16  lg:w-28 lg:h-28"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                >
                  <path
                    fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zM9.555 7.168A1 1 0 008 8v4a1 1 0 001.555.832l3-2a1 1 0 000-1.664l-3-2z"
                    clipRule="evenodd"
                  />
                </svg>
                <span className="sr-only">Play Video</span>
              </button>
            )}
            {playVideo && (
              <iframe
                src="https://www.youtube.com"
                title="YouTube video player"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen
              ></iframe>
            )}
          </div>
        </div>
      </Container>
    </div>
  );
};

export default Video;
