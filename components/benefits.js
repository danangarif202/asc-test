import Image from "next/image";
import React from "react";
import Container from "./container";

const Benefits = (props) => {
  const { data } = props;
  return (
    <div className="bg-white">
      <Container className="max-w-5xl flex flex-wrap mb-20 lg:gap-20 lg:flex-nowrap">
        <div
          className={`flex items-center justify-center w-full lg:w-1/2 ${
            props.imgPos === "right" ? "lg:order-1" : ""
          }`}
        >
          <div className="lex-shrink-0 overflow-hidden rounded-2xl h-5/6 w-96">
            <Image
              src={data.image}
              width="421"
              height="auto"
              alt="Benefits"
              className={"object-cover"}
              placeholder="blur"
              blurDataURL={data.image.src}
            />
          </div>
        </div>

        <div
          className={`flex flex-wrap items-center w-full lg:w-1/2 ${
            data.imgPos === "right" ? "lg:justify-end" : ""
          }`}
        >
          <div>
            <div className="flex flex-col w-full mt-4">
              <h3 className="max-w-2xl mt-3 text-3xl font-bold leading-snug tracking-tight text-gray-800 lg:leading-tight lg:text-4xl">
                Navigating Your Real Estate Journey
              </h3>

              <p className="max-w-2xl py-4 text-lg leading-normal text-gray-500 lg:text-xl xl:text-lg">
                look for an agency with a proven track record of success in
                buying, selling, or renting properties. you want an agency that
                has been in the industry for a while and has experience dealing
                with various types of properties.
              </p>
            </div>
            <div className="mt-5">
              <a
                href="/"
                target="_blank"
                className="px-8 py-4 text-lg font-medium text-center text-white bg-black rounded-md"
              >
                Contact Us
              </a>
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
};

export default Benefits;
