import benefitTwoImg from "../public/img/assets1.jpeg";

const benefit = {
  title: "Offer more benefits here",
  desc: "You can use this same layout with a flip image to highlight your rest of the benefits of your product. It can also contain an image or Illustration as above section along with some bullet points.",
  image: benefitTwoImg,
};

export { benefit };
