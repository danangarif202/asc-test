import Image from "next/image";
import React from "react";
import Container from "./container";

import userOneImg from "../public/img/user1.jpg";

const Testimonials = () => {
  return (
    <div className="bg-white">
      <Container className="mb-28">
        <div className="text-center mb-16">
          <h2 className="mt-3 text-3xl font-bold leading-snug tracking-tight text-gray-800 lg:leading-tight lg:text-4xl">
            Kind Words From Our Happy Customers
          </h2>
        </div>
        <div className="grid gap-10 lg:grid-cols-2 xl:grid-cols-3">
          {[1, 2, 3].map(() => (
            <div className="lg:col-span-2 xl:col-auto">
              <div className="flex flex-col justify-between w-full h-full text-gray-500 bg-white px-14 rounded-2xl py-14 border">
                <p className="text-lg leading-normal ">
                  from the moment i contacted them, they were professional,
                  knowledgeable, and attentive to my needs. they took the time
                  to listen to my preferences and privided me with a wide range
                  of properties that met my criteria.
                </p>
                <Avatar
                  image={userOneImg}
                  name="Arefin shuvo"
                  title="CEO, NoodBrew"
                />
              </div>
            </div>
          ))}
        </div>
      </Container>
    </div>
  );
};

function Avatar(props) {
  return (
    <div className="flex items-center mt-8 space-x-3">
      <div className="flex-shrink-0 overflow-hidden rounded-full">
        <Image
          src={props.image}
          width="70"
          height="70"
          alt="Avatar"
          placeholder="blur"
        />
      </div>
      <div>
        <div className="text-lg text-black font-bold">{props.name}</div>
        <div className="text-gray-600">{props.title}</div>
      </div>
    </div>
  );
}

export default Testimonials;
