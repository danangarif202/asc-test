import Link from "next/link";

const Navbar = () => {
  const navigation = ["Home", "Rent", "Sell", "About"];

  return (
    <div className="w-full bg-[#f8f9fb]">
      <nav className="container relative flex flex-wrap items-center justify-between p-8 mx-auto lg:justify-between xl:px-10">
        {/* menu  */}
        <div className="hidden text-center lg:flex lg:items-center">
          <div className="mr-20">Logo</div>
          <ul className="items-center justify-end flex-1 pt-6 list-none lg:pt-0 lg:flex">
            {navigation.map((menu, index) => (
              <li className="mr-3 nav__item" key={index}>
                <Link
                  href="/"
                  className="inline-block px-4 py-2 text-md font-normal text-gray-500 no-underline rounded-md hover:text-black focus:text-black focus:bg-black focus:outline-none"
                >
                  {menu}
                </Link>
              </li>
            ))}
          </ul>
        </div>
        <div className="hidden mr-3 space-x-4 lg:flex items-center	nav__item">
          Log In
          <Link
            href="/"
            className="px-6 py-2 text-white bg-black rounded-md md:ml-5"
          >
            Create Account
          </Link>
        </div>
      </nav>
    </div>
  );
};

export default Navbar;
