import Image from "next/image";
import React from "react";
import Container from "./container";
import {
  MapPinIcon,
  AdjustmentsHorizontalIcon,
  SunIcon,
} from "@heroicons/react/24/outline";

const Testimonials = (props) => {
  const { data } = props;
  return (
    <div className="bg-white">
      <Container className="mb-28 grid gap-10 lg:grid-cols-2 xl:grid-cols-3 max-w-6xl">
        {data.map((e, index) => (
          <div className="lg:col-span-2 xl:col-auto rounded-lg overflow-hidden shadow-lg">
            <div className="">
              <Image
                alt="The City"
                src={
                  "https://cdn.idntimes.com/content-images/post/20160723/gedung10-3015ad3b61c9fd03e414516a5d4e1015.jpg"
                }
                width={250}
                height={250}
                layout="responsive"
              />
            </div>
            <div class="px-6 py-6">
              <div class="font-bold text-2xl mb-2 text-blue-500">
                $50,200,00
              </div>
              <p class="text-gray-700 text-base">The Bismillah Apartment</p>
            </div>
            <div className="px-6 border-t pt-6 flex text-sm">
              <MapPinIcon className="w-5 mr-1" />
              Noakhali, Bangladesh
            </div>
            <div class="flex px-6 pt-4 pb-6 text-sm">
              <p className="w-1/3">{e.stock} Bed</p>
              <p className="w-1/3">2 Bath</p>
              <p className="w-1/3">1 Parking</p>
            </div>
          </div>
        ))}
      </Container>
    </div>
  );
};

export default Testimonials;
