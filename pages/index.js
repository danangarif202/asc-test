import React, { useEffect } from "react";
import Head from "next/head";
import Hero from "../components/hero";
import Navbar from "../components/navbar";
import SectionTitle from "../components/sectionTitle";
import { benefit } from "../components/data";
import Video from "../components/video";
import Benefits from "../components/benefits";
import Footer from "../components/footer";
import Testimonials from "../components/testimonials";
import Products from "../components/products";

const Home = ({ data }) => {
  useEffect(() => {
    localStorage.theme = "light";
    document.documentElement.classList.remove("dark");
  }, []);

  return (
    <>
      <Head>
        <title>ASC Web</title>
        <meta name="description" content="ASC Web" />
        <link rel="icon" href="/favicon.ico" />
        <link
          href="https://fonts.googleapis.com/css?family=Ubuntu"
          rel="stylesheet"
        />
      </Head>

      <Navbar />
      <Hero />
      <Benefits imgPos="right" data={benefit} />
      <Video />
      <SectionTitle title="Our Popular Property">
        look for an agency with a proven track record of success in buying,
        selling, or renting properties. you want an agency that has been in the
        industry for a while
      </SectionTitle>
      <Products data={data.products} />
      <Testimonials />
      <Footer />
    </>
  );
};

export default Home;

export async function getServerSideProps() {
  const res = await fetch(`https://dummyjson.com/products?limit=6`);
  const data = await res.json();
  return { props: { data } };
}
